local playerVehiclesMap = {}
local playerVehiclesList = {}
local lastVehicle = 0
local selectedVehicle = 0

function addPlayerVehicle(vehName, veh)
	lastVehicle = veh
	local tab = playerVehiclesMap[vehName]
	if tab == nil then
		tab = {}
		playerVehiclesMap[vehName] = tab
		table.insert(playerVehiclesList, vehName)
	end
	table.insert(tab, veh)
end

function getPlayerVehicle()
	if isVehicleInWorld(selectedVehicle) then
		return selectedVehicle
	end

	local playerPed = PlayerPedId()
	local veh = GetVehiclePedIsIn(playerPed, false)
	if isVehicleInWorld(veh) then
		lastVehicle = veh
		return veh
	end

	return lastVehicle
end

function isPlayerVehicle(vehName, veh)
	local tab = playerVehiclesMap[vehName]
	if tab then
		for k,v in pairs(tab) do
			if v == veh then
				return true
			end
		end
	end

	return false
end

function matchVehiclePattern(vehPat, numPat)
	local m = {}
	for ki,vi in pairs(playerVehiclesList) do
		if isMatch(vi, vehPat) then
			for kj,vj in pairs(playerVehiclesMap[vi]) do
				if isMatch(kj, numPat) then
					table.insert(m, vj)
				else
				end
			end
		end
	end

	return m
end

function selectVehicle(vehName, num)
	if vehName then
		local table = playerVehiclesMap[vehName]
		if table then
			local n = num or #table
			local veh = table[n]
			if veh then
				local disp = getVehicleDisplayName(veh)
				tellPlayer("Selected " .. disp .. " " .. n .. ".")
			end
			selectedVehicle = veh
		end
	else
		selectedVehicle = 0
		tellPlayer("Selection cleared.")
	end
end

function getDisplayVehicleCount(vehName, count)
	if count == nil or count == 0 then
		return ""
	end
	return string.format("[%2d] %s", count, vehName)
end

function tellPlayerVehicleCount(vehName)
	local tab = playerVehiclesMap[vehName]
	local count = tab and #tab or 0
	if count > 0 then
		tellPlayer(getDisplayVehicleCount(vehName, count))
	end

	return count
end

function tellPlayerAllVehicleCounts()
	local count = 0
	for k,v in pairs(playerVehiclesList) do
		count = count + tellPlayerVehicleCount(v)
	end

	return count
end

function removePlayerVehicle(veh)
	local vehName = getVehicleModelName(veh)
	local t = playerVehiclesMap[vehName]

	if t == nil then
		return
	end

	for k,v in pairs(t) do
		if veh == v then
			table.remove(t, k)
			if selectedVehicle == veh then
				selectedVehicle = 0
				tellPlayer("Cleared selected vehicle.")
			end
		end
	end

	if lastVehicle == veh then
		lastVehicle = 0
	end
end

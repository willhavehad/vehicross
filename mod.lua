MOD = {}
MOD.spoiler = 0
MOD.front_bumper = 1
MOD.rear_bumper = 2
MOD.side_skirt = 3
MOD.exhaust = 4
MOD.frame = 5
MOD.grill = 6
MOD.hood = 7
MOD.fender = 8
MOD.right_fender = 9
MOD.roof = 10
MOD.engine = 11
MOD.brakes = 12
MOD.transmission = 13
MOD.horn = 14
MOD.suspension = 15
MOD.armor = 16
MOD.front_wheel = 23
MOD.back_wheel = 24
MOD.plate_holder = 25
MOD.trim = 27
MOD.ornament = 28
MOD.dial = 30
MOD.steering_wheel = 33
MOD.shift_lever = 34
MOD.plaque = 35
MOD.hydraulics = 38
MOD.livery = 48

local MOD_NAME = {}
for k,v in pairs(MOD) do
	MOD_NAME[v] = k
end

local function getModDisplayName(veh, mod)
	return GetModSlotName(veh, mod) .. " (" .. mod .. ", " .. MOD_NAME[mod] .. ")"
end

local function getModValueDisplayName(veh, mod, value)
	return GetLabelText(GetModTextLabel(veh, mod, value)) .. " (" .. value .. ")"
end

local function getModValue(veh, mod, valueName)
	for i=0,GetNumVehicleMods(veh, mod) do
		local label = GetModTextLabel(veh, mod, i)
		if label == valueName then
			return value
		end
	end
	return -1
end

function modVehicle(veh, mod, value)
        SetVehicleModKit(veh, 0)
        SetVehicleMod(veh, mod, value)
end

function getMod(veh, mod)
	local modName = getModDisplayName(veh, mod)
	value = GetVehicleMod(veh, mod)
	local valueName = getModValueDisplayName(veh, mod, value)
	return modName, valueName
end

function setMod(veh, mod, value)
	value = tonumber(value) or getModValue(veh, mod, value)
	modVehicle(veh, mod, value)
end

function cmdMod(veh, mod, value)
	if value then
		setMod(veh, mod, value)
	end

	return getMod(veh, mod)
end

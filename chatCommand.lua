RegisterCommand("c", function(source, args, rawCommand)
	createVehicle(args[1])
end)

RegisterCommand("fix", function(source, args, rawCommand)
	fixVehicle(getPlayerVehicle())
end)

RegisterCommand("dirt", function(source, args, rawCommand)
	dirtyVehicle(getPlayerVehicle(), arg[1])
end)

RegisterCommand("wash", function(source, args, rawCommand)
	washVehicle(getPlayerVehicle())
end)

RegisterCommand("dv", function(source, args, rawCommand)
	if args[1] then
		if args[2] then
			local m = matchVehiclePattern(args[1], args[2])
			local msg
			if #m == 0 then
				msg = "No vehicles to delete."
			elseif #m == 1 then
				msg = getDeleteMessage(m[1])
			else
				local n = {}
				for k,v in pairs(m) do
					local vehName = getVehicleDisplayName(v)
					n[vehName] = (n[vehName] or 0) + 1
				end

				msg = "Deleted"
				for k,v in pairs(n) do
					msg = msg .. " " .. v .. " " .. k .. ","
				end
				msg = msg:sub(1, -2) .. "."
			end
			for k,v in pairs(m) do
				deleteVehicle(v)
			end
			tellPlayer(msg)
		else
			tellPlayer("usage: dv [<car-pattern> <number-pattern>]")
		end
	else
		local veh = getPlayerVehicle()
		local msg = getDeleteMessage(veh)
		if deleteVehicle(veh) then
			tellPlayer(msg)
		else
			tellPlayer("No vehicle to delete.")
		end
	end
end)

RegisterCommand("add", function(source, args, rawCommand)
	local playerPed = PlayerPedId()
	local veh = GetVehiclePedIsIn(playerPed, false)
	if isVehicleInWorld(veh) then
		local vehName = getVehicleModelName(veh)
		if not isPlayerVehicle(vehName, veh) then
			addPlayerVehicle(vehName, veh)
			tellPlayer("Added " .. getVehicleDisplayName(veh))
		else
			tellPlayer("Vehicle already added.")
		end
	else
		tellPlayer("No vehicle to add.")
	end
end)

RegisterCommand("sel", function(source, args, rawCommand)
	selectVehicle(args[1], args[2])
end)

RegisterCommand("ls", function(source, args, rawCommand)
	if args[1] then
		if tellPlayerVehicleCount(args[1]) == 0 then
			tellPlayer("No " .. args[1] .. " assigned.")
		end
	else
		if tellPlayerAllVehicleCounts() == 0 then
			tellPlayer("No vehicles assigned.")
		end
	end
end)

RegisterCommand("mod", function(source, args, rawCommand)
	if args[1] == nil then
		tellPlayer("usage: mod <modNumber>|<modName> [<valueNumber>|<valueName>]")
		return
	end

	local veh = getPlayerVehicle()
	if isVehicleInWorld(veh) then
		local mod = tonumber(args[1]) or mods[args[1]]
		local modName,valueName = cmdMod(veh, mod, args[2])
		tellPlayer(modName .. " = " .. valueName)
	else
		tellPlayer("No vehicle to mod.")
	end
end)

for k,v in pairs(MOD) do
	RegisterCommand(k, function(source, args, rawCommand)
		local veh = getPlayerVehicle()
		if isVehicleInWorld(veh) then
			local modName,valueName = cmdMod(veh, v, args[1])
			tellPlayer(modName .. " = " .. valueName)
		else
			tellPlayer("No vehicle to mod.")
		end
	end)
end

RegisterCommand("plate", function(source, args, rawCommand)
	local t = args[1]
	for i = 2,#args do
		t = t .. " " .. args[i]
	end
	SetVehicleNumberPlateText(getPlayerVehicle(), t)
end)

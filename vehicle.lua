local defaultVehName = "freecrawler"
local timeout = 5000
local waitInc = 100

function getVehicleModelName(veh)
	return GetDisplayNameFromVehicleModel(GetEntityModel(veh)):lower()
end

function getVehicleDisplayName(veh)
	-- TODO: Get make/model
	return getVehicleModelName(veh)
end

function isVehicleInWorld(veh)
	return DoesEntityExist(veh)
end

function createVehicle(modelName)
	local vehName = modelName or defaultVehName

	local playerPed = PlayerPedId()
	local x,y,z = table.unpack(GetEntityCoords(playerPed))
	local heading = GetEntityHeading(playerPed)

	local vehicleHash = GetHashKey(vehName)
	RequestModel(vehicleHash)

	Citizen.CreateThread(function()
		local time = 0
		while not HasModelLoaded(vehicleHash) do
			time = time + waitInc
			Citizen.Wait(waitInc)
			if time > timeout then
				tellPlayer("Failed to create " .. vehName .. " (timeout)")
				return
			end
		end

		local veh = CreateVehicle(vehicleHash, x, y, z, heading, true, false)
		if isVehicleInWorld(veh) then
			addPlayerVehicle(vehName, veh)
			tellPlayer("Created " .. getVehicleDisplayName(veh) .. ".")
		else
			tellPlayer("Failed to create " .. vehName .. " (did not create)")
		end
	end)
end

function fixVehicle(veh)
	SetVehicleFixed(veh)
end

function dirtyVehicle(veh, level)
	level = tofloat(level)
	SetVehicleDirtLevel(veh, level)
end

function washVehicle(veh, level)
	WashDecalsFromVehicle(veh, 1.0)
end

function deleteVehicle(veh)
	if isVehicleInWorld(veh) then
		local vehName = getVehicleModelName(veh)
		removePlayerVehicle(veh)
		SetEntityAsMissionEntity(veh, true, true)
		DeleteVehicle(veh)
		return true
	else
		return false
	end
end

function getDeleteMessage(veh)
	return "Deleted " .. getVehicleDisplayName(veh) .. "."
end

function tellPlayer(msg)
	TriggerEvent('chat:addMessage', { args = { msg } })
end

function isMatch(str, pat)
	local s = tostring(str)
	return tostring(str):match("^" .. pat .. "$")
end

function tofloat(str)
	return str and (tonumber(str) + 0.0) or 0.0
end
